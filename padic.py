from fractions import Fraction
from modp import *
from copy import deepcopy as dc

maxint = 2147483647

class PAdic:	# Базовый класс p-адических чисел
    
    def __init__(self, p):
        self.p = p
        self.val  = '' 	# Известная часть p-адического числа
        self.prec = 0 	# Текущая точность
        self.order = 0 	# Порядок
        pass
    
    def get(self, prec, decimal = True):	# Функция для получения числа с заданой точностью
        while self.prec < prec:
            self.val = str(int(self._nextdigit())) + self.val
            self.prec += 1
        if self.order < 0:
            return (self.val[:self.order] + ('.' if decimal else '') + self.val[self.order:])[-prec-1:]
        return (self.val + self.order * '0')[-prec:]
    
    def _nextdigit(self):	# Функция для получения очередной цифры p-адического числа
        raise NotImplementedError
    
    def getdigit(self, index):	# Функция возвращает цифру числа по индексу
        tmp = self.get(index + 1, False)
        return int(tmp[0])
    
    # Переопределение перевода данного класса в другие с точностью 32
    def __int__(self):
        return int(self.get(32), self.p)
    def __str__(self):
        return self.get(32)
    
    # Переопределение базовых арифметических операций
    def __invert__(self):
        return PAdicNeg(self.p, dc(self))
    def __neg__(self):
        return PAdicNeg(self.p, dc(self))
    def __add__(self, other):
        return PAdicAdd(self.p, dc(self), dc(other))
    def __radd__(self, other):
        return PAdicAdd(self.p, dc(other), dc(self))
    def __sub__(self, other):
        return PAdicAdd(self.p, dc(self), PAdicNeg(self.p, dc(other)))
    def __rsub__(self, other):
        return PAdicAdd(self.p, dc(other), PAdicNeg(self.p, dc(self)))
    def __mul__(self, other):
        return PAdicMul(self.p, dc(self), dc(other))
    def __rmul__(self, other):
        return PAdicMul(self.p, dc(other), dc(self))
    def __and__(self, other):
        return PAdicAnd(self.p, dc(self), dc(other))
    def __rand__(self, other):
        return PAdicAnd(self.p, dc(other), dc(self))
    def __or__(self, other):
        return PAdicOr(self.p, dc(self), dc(other))
    def __ror__(self, other):
        return PAdicOr(self.p, dc(other), dc(self))
    
    # Переопределение метода abs()
    def __abs__(self):
        if self.order == maxint:
            return 0
        numer = denom = 1
        if self.order > 0:
            numer = self.p ** self.order
        if self.order < 0:
            denom = self.p ** self.order
        return Fraction(numer, denom)
	
	# Функция для получения первых k бит p-адического числа
    def modpk(self, k):
        return (self & PAdicConst(self.p, self.p ** k - 1))

class PAdicConst(PAdic):	# Класс, описывающий p-адическую константу
    
    def __init__(self, p, value):
        PAdic.__init__(self, p)
        value = Fraction(value)
        
        if value == 0:
            self.value  = value
            self.val    = '0'
            self.order  = maxint
            self.zero   = True
            return
        
        self.order = 0
        while not value.numerator % self.p:
            self.order += 1
            value /= self.p
        while not value.denominator % self.p:
            self.order -= 1
            value *= self.p
        self.value = value
        
        self.zero = not value
    
    def get(self, prec, decimal = True):
        if self.zero:
            return '0' * prec
        return PAdic.get(self, prec, decimal)
    
    def _nextdigit(self):
        rem = ModP(self.p, self.value.numerator) / ModP(self.p, self.value.denominator)
        self.value -= int(rem)
        self.value /= self.p
        return rem

class PAdicAdd(PAdic):	# Класс, описывающий сумму двух p-адических чисел
    
    def __init__(self, p, arg1, arg2):
        PAdic.__init__(self, p)
        self.carry = 0
        self.arg1 = arg1
        self.arg2 = arg2
        self.order = self.prec = min(arg1.order, arg2.order)
        arg1.order -= self.order
        arg2.order -= self.order
        self.index = 0
        digit = self._nextdigit()
        while digit == 0 and self.order < 64:
            self.order += 1
            digit = self._nextdigit()
        self.val += str(int(digit))
        self.prec = 1
    
    def _nextdigit(self):
        s = self.arg1.getdigit(self.index) + self.arg2.getdigit(self.index) + self.carry
        self.carry = s // self.p
        self.index += 1
        return s % self.p 

class PAdicNeg(PAdic):	# Класс, описывающий отрицание p-адического числа
    
    def __init__(self, p, arg):
        PAdic.__init__(self, p)
        self.arg = arg
        self.order = 0
    
    def _nextdigit(self):
        return self.p - 1 - self.arg.getdigit(self.prec)

class PAdicMul(PAdic):	# Класс, описывающий умножение двух p-адических чисел
    
    def __init__(self, p, arg1, arg2):
        PAdic.__init__(self, p)
        self.carry = 0
        self.arg1 = arg1
        self.arg2 = arg2
        self.order = arg1.order + arg2.order
        self.arg1.order = self.arg2.order = 0
        self.index = 0
    
    def _nextdigit(self):
        s = sum(self.arg1.getdigit(i) * self.arg2.getdigit(self.index - i) for i in range(self.index + 1)) + self.carry
        self.carry = s // self.p
        self.index += 1
        return s % self.p

class PAdicAnd(PAdic):	# Класс, описывающий побитовую операцию И для двух p-адических чисел

    def __init__(self, p, arg1, arg2):
        PAdic.__init__(self, p)
        self.arg1 = arg1
        self.arg2 = arg2
        self.order = self.prec = min(arg1.order, arg2.order)
        arg1.order -= self.order
        arg2.order -= self.order
        self.index = 0
        digit = self._nextdigit()
        while digit == 0 and self.order < 64:
            self.order += 1
            digit = self._nextdigit()
        self.val += str(int(digit))
        self.prec = 1

    def _nextdigit(self):
        s = self.arg1.getdigit(self.index) & self.arg2.getdigit(self.index)
        self.index += 1
        return s % self.p

class PAdicOr(PAdic):	# Класс, описывающий побитовую операцию ИЛИ для двух p-адических чисел

    def __init__(self, p, arg1, arg2):
        PAdic.__init__(self, p)
        self.arg1 = arg1
        self.arg2 = arg2
        self.order = self.prec = min(arg1.order, arg2.order)
        arg1.order -= self.order
        arg2.order -= self.order
        self.index = 0
        digit = self._nextdigit()
        while digit == 0 and self.order < 64:
            self.order += 1
            digit = self._nextdigit()
        self.val += str(int(digit))
        self.prec = 1

    def _nextdigit(self):
        s = self.arg1.getdigit(self.index) | self.arg2.getdigit(self.index)
        self.index += 1
        return s % self.p
