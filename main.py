#Net

from padic import PAdicConst
import matplotlib.pyplot as plt
import pandas as pd

def function(x):        # Функция Климова-Шамира

# Функция, использующаяся для отображения 2-адического числа на графике
def toCoordinate(x, k): # Функция Климова-Шамира
    tmp = x
    tmp = tmp.modpk(k)
    tmp = tmp * PAdicConst(2, 1 / (x.p ** k))
    return tmp.get(k)

prec = int(input("prec = ")) 	# Точность

x = []	# Список для x в 2-адическом виде
y = [] 	# Список для f(x) в 2-адическом виде

for i in range(2 ** prec):		# Получение x и f(x)
    tmp = PAdicConst(2, i)
    x.append(toCoordinate(tmp, prec))
    y.append(toCoordinate(function(tmp), prec))

xplt = []	# Список для x в 10 системе счисления
yplt = []	# Список для f(x) в 10 системе счисления


for i in range(2 ** prec):  # Получение x и f(x) в 10 системе счисления
    
    tmp = 1.0
    tmpx = 0.0
    rang = x[i].find('.') + 1
    for j in range(rang, len(x[i])):
        tmp /= 2
        if (x[i][j] == '1'):
            tmpx += tmp
            
    tmp = 1.0
    tmpy = 0.0
    rang = y[i].find('.') + 1
    for j in range(rang, len(y[i])):
        tmp /= 2
        if (y[i][j] == '1'):
            tmpy += tmp

    xplt.append(tmpx)
    yplt.append(tmpy)

# Сохранение данных в файл в формате .xlsx
data = {'i': list(range(2 ** prec)), 'x': xplt, 'f(x)': yplt}
cols = ['i', 'x', 'f(x)']
df = pd.DataFrame(data)
df.to_excel("./DataFiles/Data" + str(prec) + ".xlsx", index = False, header = cols)

# Сохранение графика в файл в формате .png
plt.title("f(x) = ")
plt.xlabel("x", fontsize = 14)
plt.ylabel("f(x)", fontsize = 14)
plt.grid(which="major", linestyle=":", color="black", linewidth=0.5)
plt.scatter(xplt, yplt, c = "red", marker = '.', sizes=[10,10])
plt.savefig("./Graphs/Graph" + str(prec) + ".png", dpi = 800)

